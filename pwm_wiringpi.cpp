#include <stdio.h>
#include <stdlib.h>
#include <wiringPi.h>
#include <unistd.h>
#include <iostream>

using namespace std;

#define LED01 12

int main(void) {
    int rc;
    int value;
    const int range=1024;
    puts("Lets LED...");
  
    if ((rc = wiringPiSetupGpio()) == 0) {
        puts("gpio OK");
        pinMode(LED01, PWM_OUTPUT);

        // set the PWM mode to MS type
        pwmSetMode(PWM_MODE_MS);

        // range & divide down clock
        pwmSetRange(range);
        pwmSetClock(10);

        while (true){
			for (value = 0; value < range; value++){
			  pwmWrite(LED01, value);
			  delay(1);
			}
			for (value = range-1; value > -1; value--){
			  pwmWrite(LED01, value);
			  delay(1);
			}
        }//while
		
    } else {
        std::cout << "Library setup failed with code: " << rc << std::endl;
        return rc;
    }

    return EXIT_SUCCESS;
}
