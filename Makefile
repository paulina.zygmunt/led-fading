CXXFLAGS =	-O2 -g -Wall -fmessage-length=0

OBJS =		pwm_wiringpi.o

LIBS =		-lwiringPi

TARGET =	pwm_wiringpi

$(TARGET):	$(OBJS)
	$(CXX) -o $(TARGET) $(OBJS) $(LIBS)

all:	$(TARGET)

clean:
	rm -f $(OBJS) $(TARGET)
